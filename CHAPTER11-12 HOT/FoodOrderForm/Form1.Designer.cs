﻿namespace FoodOrderForm
{
    partial class FoodOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.askingName = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.askingBurger = new System.Windows.Forms.Label();
            this.burgerCheckBox = new System.Windows.Forms.CheckBox();
            this.BurgerTopping = new System.Windows.Forms.Label();
            this.onionsCheckBox = new System.Windows.Forms.CheckBox();
            this.picklesCheckBox = new System.Windows.Forms.CheckBox();
            this.cheeseCheckBox = new System.Windows.Forms.CheckBox();
            this.burgerErrorLabel = new System.Windows.Forms.Label();
            this.askingDrink = new System.Windows.Forms.Label();
            this.drinkCheckBox = new System.Windows.Forms.CheckBox();
            this.drinkMixins = new System.Windows.Forms.Label();
            this.cherryCheckBox = new System.Windows.Forms.CheckBox();
            this.vanillaCheckBox = new System.Windows.Forms.CheckBox();
            this.orangeCheckBox = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.OrderTextBox = new System.Windows.Forms.TextBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // askingName
            // 
            this.askingName.AutoSize = true;
            this.askingName.Location = new System.Drawing.Point(12, 18);
            this.askingName.Name = "askingName";
            this.askingName.Size = new System.Drawing.Size(189, 13);
            this.askingName.TabIndex = 0;
            this.askingName.Text = "What name do you want on the order?";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(12, 34);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(186, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 2;
            // 
            // askingBurger
            // 
            this.askingBurger.AutoSize = true;
            this.askingBurger.Location = new System.Drawing.Point(12, 86);
            this.askingBurger.Name = "askingBurger";
            this.askingBurger.Size = new System.Drawing.Size(115, 13);
            this.askingBurger.TabIndex = 3;
            this.askingBurger.Text = "Do you want a burger?";
            // 
            // burgerCheckBox
            // 
            this.burgerCheckBox.AutoSize = true;
            this.burgerCheckBox.Location = new System.Drawing.Point(12, 102);
            this.burgerCheckBox.Name = "burgerCheckBox";
            this.burgerCheckBox.Size = new System.Drawing.Size(44, 17);
            this.burgerCheckBox.TabIndex = 4;
            this.burgerCheckBox.Text = "Yes";
            this.burgerCheckBox.UseVisualStyleBackColor = true;
            // 
            // BurgerTopping
            // 
            this.BurgerTopping.AutoSize = true;
            this.BurgerTopping.Location = new System.Drawing.Point(12, 137);
            this.BurgerTopping.Name = "BurgerTopping";
            this.BurgerTopping.Size = new System.Drawing.Size(82, 13);
            this.BurgerTopping.TabIndex = 5;
            this.BurgerTopping.Text = "BurgerToppings";
            // 
            // onionsCheckBox
            // 
            this.onionsCheckBox.AutoSize = true;
            this.onionsCheckBox.Location = new System.Drawing.Point(12, 153);
            this.onionsCheckBox.Name = "onionsCheckBox";
            this.onionsCheckBox.Size = new System.Drawing.Size(59, 17);
            this.onionsCheckBox.TabIndex = 6;
            this.onionsCheckBox.Text = "Onions";
            this.onionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // picklesCheckBox
            // 
            this.picklesCheckBox.AutoSize = true;
            this.picklesCheckBox.Location = new System.Drawing.Point(12, 176);
            this.picklesCheckBox.Name = "picklesCheckBox";
            this.picklesCheckBox.Size = new System.Drawing.Size(60, 17);
            this.picklesCheckBox.TabIndex = 7;
            this.picklesCheckBox.Text = "Pickles";
            this.picklesCheckBox.UseVisualStyleBackColor = true;
            // 
            // cheeseCheckBox
            // 
            this.cheeseCheckBox.AutoSize = true;
            this.cheeseCheckBox.Location = new System.Drawing.Point(12, 199);
            this.cheeseCheckBox.Name = "cheeseCheckBox";
            this.cheeseCheckBox.Size = new System.Drawing.Size(62, 17);
            this.cheeseCheckBox.TabIndex = 8;
            this.cheeseCheckBox.Text = "Cheese";
            this.cheeseCheckBox.UseVisualStyleBackColor = true;
            // 
            // burgerErrorLabel
            // 
            this.burgerErrorLabel.AutoSize = true;
            this.burgerErrorLabel.Location = new System.Drawing.Point(12, 219);
            this.burgerErrorLabel.Name = "burgerErrorLabel";
            this.burgerErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.burgerErrorLabel.TabIndex = 9;
            // 
            // askingDrink
            // 
            this.askingDrink.AutoSize = true;
            this.askingDrink.Location = new System.Drawing.Point(166, 86);
            this.askingDrink.Name = "askingDrink";
            this.askingDrink.Size = new System.Drawing.Size(108, 13);
            this.askingDrink.TabIndex = 10;
            this.askingDrink.Text = "Do you want a drink?";
            // 
            // drinkCheckBox
            // 
            this.drinkCheckBox.AutoSize = true;
            this.drinkCheckBox.Location = new System.Drawing.Point(169, 102);
            this.drinkCheckBox.Name = "drinkCheckBox";
            this.drinkCheckBox.Size = new System.Drawing.Size(44, 17);
            this.drinkCheckBox.TabIndex = 11;
            this.drinkCheckBox.Text = "Yes";
            this.drinkCheckBox.UseVisualStyleBackColor = true;
            // 
            // drinkMixins
            // 
            this.drinkMixins.AutoSize = true;
            this.drinkMixins.Location = new System.Drawing.Point(166, 137);
            this.drinkMixins.Name = "drinkMixins";
            this.drinkMixins.Size = new System.Drawing.Size(64, 13);
            this.drinkMixins.TabIndex = 12;
            this.drinkMixins.Text = "Drink Mixins";
            // 
            // cherryCheckBox
            // 
            this.cherryCheckBox.AutoSize = true;
            this.cherryCheckBox.Location = new System.Drawing.Point(169, 153);
            this.cherryCheckBox.Name = "cherryCheckBox";
            this.cherryCheckBox.Size = new System.Drawing.Size(56, 17);
            this.cherryCheckBox.TabIndex = 13;
            this.cherryCheckBox.Text = "Cherry";
            this.cherryCheckBox.UseVisualStyleBackColor = true;
            // 
            // vanillaCheckBox
            // 
            this.vanillaCheckBox.AutoSize = true;
            this.vanillaCheckBox.Location = new System.Drawing.Point(169, 176);
            this.vanillaCheckBox.Name = "vanillaCheckBox";
            this.vanillaCheckBox.Size = new System.Drawing.Size(57, 17);
            this.vanillaCheckBox.TabIndex = 14;
            this.vanillaCheckBox.Text = "Vanilla";
            this.vanillaCheckBox.UseVisualStyleBackColor = true;
            // 
            // orangeCheckBox
            // 
            this.orangeCheckBox.AutoSize = true;
            this.orangeCheckBox.Location = new System.Drawing.Point(169, 199);
            this.orangeCheckBox.Name = "orangeCheckBox";
            this.orangeCheckBox.Size = new System.Drawing.Size(61, 17);
            this.orangeCheckBox.TabIndex = 15;
            this.orangeCheckBox.Text = "Orange";
            this.orangeCheckBox.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(166, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 16;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 247);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 17;
            this.addButton.Text = "button1";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // OrderTextBox
            // 
            this.OrderTextBox.Location = new System.Drawing.Point(280, 34);
            this.OrderTextBox.Multiline = true;
            this.OrderTextBox.Name = "OrderTextBox";
            this.OrderTextBox.ReadOnly = true;
            this.OrderTextBox.Size = new System.Drawing.Size(192, 198);
            this.OrderTextBox.TabIndex = 18;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(415, 247);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(34, 13);
            this.priceLabel.TabIndex = 19;
            this.priceLabel.Text = "Total:";
            // 
            // FoodOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 274);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.OrderTextBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.orangeCheckBox);
            this.Controls.Add(this.vanillaCheckBox);
            this.Controls.Add(this.cherryCheckBox);
            this.Controls.Add(this.drinkMixins);
            this.Controls.Add(this.drinkCheckBox);
            this.Controls.Add(this.askingDrink);
            this.Controls.Add(this.burgerErrorLabel);
            this.Controls.Add(this.cheeseCheckBox);
            this.Controls.Add(this.picklesCheckBox);
            this.Controls.Add(this.onionsCheckBox);
            this.Controls.Add(this.BurgerTopping);
            this.Controls.Add(this.burgerCheckBox);
            this.Controls.Add(this.askingBurger);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.askingName);
            this.Name = "FoodOrderForm";
            this.Text = "Food Order";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label askingName;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label askingBurger;
        private System.Windows.Forms.CheckBox burgerCheckBox;
        private System.Windows.Forms.Label BurgerTopping;
        private System.Windows.Forms.CheckBox onionsCheckBox;
        private System.Windows.Forms.CheckBox picklesCheckBox;
        private System.Windows.Forms.CheckBox cheeseCheckBox;
        private System.Windows.Forms.Label burgerErrorLabel;
        private System.Windows.Forms.Label askingDrink;
        private System.Windows.Forms.CheckBox drinkCheckBox;
        private System.Windows.Forms.Label drinkMixins;
        private System.Windows.Forms.CheckBox cherryCheckBox;
        private System.Windows.Forms.CheckBox vanillaCheckBox;
        private System.Windows.Forms.CheckBox orangeCheckBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox OrderTextBox;
        private System.Windows.Forms.Label priceLabel;
    }
}

